package com.vcroitor.mtribes.demo.ui.carlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vcroitor.mtribes.demo.ui.map.CarMapFragment
import com.vcroitor.mtribes.demo.R
import com.vcroitor.mtribes.demo.persistence.model.Car
import com.vcroitor.mtribes.demo.persistence.model.QualityType
import com.vcroitor.mtribes.demo.ui.CarListViewModel
import kotlinx.android.synthetic.main.fragment_car_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class CarListFragment : Fragment() {

    private val carViewModel: CarListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_car_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        val adapter = CarListAdapter { car -> onLocationClick(car) }
        carsRecycler.adapter = adapter

        refresh.setOnRefreshListener {
            carViewModel.load()
        }

        carViewModel.isLoadingLive.observe(viewLifecycleOwner, Observer {
            refresh.isRefreshing = it
        })

        carViewModel.filter(CarListViewModel.Filter.ALL)

        carViewModel.carList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

    }

    private fun onLocationClick(car: Car) {
        findNavController().navigate(
            R.id.action_carListFragment_to_carMapFragment,
            bundleOf(CarMapFragment.ARG_COORDINATES to car.coordinates)
        )
    }
}

class CarListAdapter(private val onLocationClick: (car: Car) -> Unit) :
    PagedListAdapter<Car, CarListViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarListViewHolder {
        return CarListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_car_item,
                parent,
                false
            ),
            onLocationClick
        )
    }

    override fun onBindViewHolder(holder: CarListViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Car>() {
            override fun areItemsTheSame(oldItem: Car, newItem: Car): Boolean {
                return oldItem.vin == newItem.vin
            }

            override fun areContentsTheSame(oldItem: Car, newItem: Car): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class CarListViewHolder(item: View, private val onLocationClick: (car: Car) -> Unit) : RecyclerView.ViewHolder(item) {

    private val name: TextView = itemView.findViewById(R.id.carName)
    private val interior: TextView = itemView.findViewById(R.id.interiorValue)
    private val exterior: TextView = itemView.findViewById(R.id.exteriorValue)
    private val engine: TextView = itemView.findViewById(R.id.engineValue)
    private val fuel: TextView = itemView.findViewById(R.id.fuelValue)
    private val vin: TextView = itemView.findViewById(R.id.vinValue)
    private val address: TextView = itemView.findViewById(R.id.addressValue)
    private val location: View = itemView.findViewById(R.id.location)

    fun bind(car: Car) {
        name.text = car.name
        bindByQuality(interior, car.interior)
        bindByQuality(exterior, car.exterior)
        engine.text = car.engineType
        fuel.text = car.fuel.toString()
        vin.text = car.vin
        address.text = car.address

        location.setOnClickListener {
            onLocationClick.invoke(car)
        }
    }

    private fun bindByQuality(view: TextView, qualityType: QualityType) {
        view.text = qualityType.value
        when (qualityType) {
            QualityType.GOOD -> {
                view.setTextColor(ContextCompat.getColor(view.context, R.color.light_green))
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.ic_thumb_up_green_24dp, 0)

            }
            QualityType.UNACCEPTABLE -> {
                view.setTextColor(ContextCompat.getColor(view.context, R.color.dark_red))
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.ic_thumb_down_dark_red_24dp, 0)

            }

            QualityType.UNKNOWN -> {
                view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            }
        }
    }
}

