package com.vcroitor.mtribes.demo.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vcroitor.mtribes.demo.persistence.dao.CarDao
import com.vcroitor.mtribes.demo.persistence.model.Car


@Database(
        entities = [Car::class],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class CarsDb : RoomDatabase() {
    abstract fun carDao(): CarDao
}