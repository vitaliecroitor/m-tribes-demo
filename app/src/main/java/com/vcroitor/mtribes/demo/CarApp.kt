package com.vcroitor.mtribes.demo

import android.app.Application
import com.vcroitor.mtribes.demo.api.CarsApi
import com.vcroitor.mtribes.demo.api.ClientBuilder
import com.vcroitor.mtribes.demo.persistence.DbBuilder
import com.vcroitor.mtribes.demo.repo.CarRepository
import com.vcroitor.mtribes.demo.repo.MainCarRepository
import com.vcroitor.mtribes.demo.ui.CarListViewModel
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module

class CarApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@CarApp)
            modules(buildModule())
        }
    }

    private fun buildModule(): Module = module {
        val database = DbBuilder.build(get())
        val retrofit = ClientBuilder.buildCarApi()
        single { retrofit }
        single { database }
        single<CarsApi> { retrofit.create(CarsApi::class.java) }
        single { database.carDao() }
        single<CarRepository> { MainCarRepository() }
        viewModel { CarListViewModel() }
    }
}