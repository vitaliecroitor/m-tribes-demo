package com.vcroitor.mtribes.demo.persistence.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Coordinates(val lat: Float = 0f, val lng: Float = 0f, val z: Float = 0f) : Parcelable