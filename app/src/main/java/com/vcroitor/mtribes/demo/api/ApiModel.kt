package com.vcroitor.mtribes.demo.api

import com.google.gson.annotations.SerializedName

data class CarListModel(@SerializedName("placemarks") val placeMarks: List<ApiCar>)

data class ApiCar(
    @SerializedName("address") val address: String,
    @SerializedName("coordinates") val coordinates: Array<Float>,
    @SerializedName("engineType") val engineType: String,
    @SerializedName("exterior") val exterior: String,
    @SerializedName("interior") val interior: String,
    @SerializedName("fuel") val fuel: Int,
    @SerializedName("name") val name: String,
    @SerializedName("vin") val vin: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ApiCar

        if (address != other.address) return false
        if (!coordinates.contentEquals(other.coordinates)) return false
        if (engineType != other.engineType) return false
        if (exterior != other.exterior) return false
        if (interior != other.interior) return false
        if (name != other.name) return false
        if (vin != other.vin) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + coordinates.contentHashCode()
        result = 31 * result + engineType.hashCode()
        result = 31 * result + exterior.hashCode()
        result = 31 * result + interior.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + vin.hashCode()
        return result
    }
}