package com.vcroitor.mtribes.demo.persistence.model

enum class QualityType(val value: String) {
    UNKNOWN("UNKNOWN"),
    GOOD("GOOD"),
    UNACCEPTABLE("UNACCEPTABLE");

    companion object {
        fun of(value: String): QualityType {
            return values().firstOrNull { it.value == value } ?: UNKNOWN
        }
    }
}