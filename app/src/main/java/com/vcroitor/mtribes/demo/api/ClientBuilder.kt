package com.vcroitor.mtribes.demo.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ClientBuilder {

    private const val CAR_API_URL = "http://data.m-tribes.com/"

    fun buildCarApi(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(CAR_API_URL).addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }
}