package com.vcroitor.mtribes.demo.repo

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.vcroitor.mtribes.demo.api.CarsApi
import com.vcroitor.mtribes.demo.persistence.dao.CarDao
import com.vcroitor.mtribes.demo.persistence.model.Car
import com.vcroitor.mtribes.demo.persistence.model.ClusterCar
import com.vcroitor.mtribes.demo.persistence.model.QualityType
import com.vcroitor.mtribes.demo.trasformers.CarTransformer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject

interface CarRepository {
    suspend fun loadCars()
    suspend fun getCarList(): List<ClusterCar>
    fun getCarListLiveData(): LiveData<PagedList<Car>>
    fun getCarListLiveDataByQuality(qualityType: QualityType): LiveData<PagedList<Car>>
}

class MainCarRepository : CarRepository, KoinComponent {

    private val carsApi: CarsApi by inject()
    private val carsDao: CarDao by inject()

    override fun getCarListLiveData(): LiveData<PagedList<Car>> {
        return carsDao.getCarListDataSource().toLiveData(PAGE_SIZE)
    }

    override fun getCarListLiveDataByQuality(qualityType: QualityType): LiveData<PagedList<Car>> {
        return carsDao.getCarListDataSourceByQuality(qualityType).toLiveData(PAGE_SIZE)
    }

    override suspend fun getCarList(): List<ClusterCar> {
        return withContext(Dispatchers.IO) {
            carsDao.getCarList()
        }
    }

    override suspend fun loadCars() {
        withContext(Dispatchers.IO) {
            val apiCars = carsApi.loadCars().await()
            val cars = apiCars.placeMarks.map {
                CarTransformer.transform(it)
            }
            carsDao.insert(cars)
        }
    }

    companion object {
        const val PAGE_SIZE = 50
    }

}
