package com.vcroitor.mtribes.demo.api

import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface CarsApi {

    @GET("/locations.json")
    fun loadCars(): Deferred<CarListModel>
}