package com.vcroitor.mtribes.demo.persistence

import android.content.Context
import androidx.room.Room

object DbBuilder {

    private const val DB_NAME = "cars.db"

    fun build(context: Context): CarsDb {
        return Room.databaseBuilder(context.applicationContext, CarsDb::class.java, DB_NAME).build()
    }
}