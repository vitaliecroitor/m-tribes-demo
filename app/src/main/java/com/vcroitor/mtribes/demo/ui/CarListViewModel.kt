package com.vcroitor.mtribes.demo.ui

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.vcroitor.mtribes.demo.persistence.model.Car
import com.vcroitor.mtribes.demo.persistence.model.ClusterCar
import com.vcroitor.mtribes.demo.persistence.model.QualityType
import com.vcroitor.mtribes.demo.repo.CarRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.inject

class CarListViewModel : ViewModel(), KoinComponent {

    private val carRepository: CarRepository by inject()

    private val _isLoading = MutableLiveData<Boolean>()

    val clusterCarList = MutableLiveData<List<ClusterCar>>()

    val carList: LiveData<PagedList<Car>>
        get() {
            return Transformations.switchMap(filterLiveData) { filter ->
                when (filter) {
                    Filter.GOOD -> carRepository.getCarListLiveDataByQuality(QualityType.GOOD)
                    else -> {
                        carRepository.getCarListLiveData()
                    }
                }
            }
        }

    val isLoadingLive: LiveData<Boolean>
        get() {
            return _isLoading
        }

    private val filterLiveData = MutableLiveData<Filter>()

    init {
        load()
    }

    fun filter(filter: Filter) {
        filterLiveData.value = filter
    }

    fun load() {
        viewModelScope.launch {
            _isLoading.value = true
            withContext(Dispatchers.IO) {
                carRepository.loadCars()
            }
            _isLoading.value = false
        }
    }

    fun getCarList() {
        viewModelScope.launch {
            val list = withContext(Dispatchers.IO) {
                carRepository.getCarList()
            }
            clusterCarList.value = list
        }
    }

    enum class Filter {
        ALL,
        GOOD
    }
}