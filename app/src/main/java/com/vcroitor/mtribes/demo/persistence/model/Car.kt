package com.vcroitor.mtribes.demo.persistence.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "cars")
data class Car(
        @PrimaryKey
        @ColumnInfo(name = "vin") val vin: String = UUID.randomUUID().toString(),

        @ColumnInfo(name = "name") val name: String = "",

        @ColumnInfo(name = "address") val address: String = "",

        @ColumnInfo(name = "exterior") val exterior: QualityType = QualityType.UNKNOWN,

        @ColumnInfo(name = "interior") val interior: QualityType = QualityType.UNKNOWN,

        @ColumnInfo(name = "fuel") val fuel: Int = 0,

        @ColumnInfo(name = "engineType") val engineType: String = "",

        @Embedded
        val coordinates: Coordinates = Coordinates()
)
