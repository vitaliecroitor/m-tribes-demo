package com.vcroitor.mtribes.demo.ui.map

import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.vcroitor.mtribes.demo.R
import com.vcroitor.mtribes.demo.persistence.model.ClusterCar
import com.vcroitor.mtribes.demo.persistence.model.Coordinates
import com.vcroitor.mtribes.demo.ui.CarListViewModel
import com.vcroitor.mtribes.demo.ui.location.LocationProvider
import org.koin.android.viewmodel.ext.android.viewModel


class CarMapFragment : Fragment(), OnMapReadyCallback {

    private val carViewModel: CarListViewModel by viewModel()

    private lateinit var clusterManager: ClusterManager<ClusterCar>
    private lateinit var map: GoogleMap
    private var userMarker: Marker? = null

    private val carCoordinates: Coordinates? by lazy { arguments?.getParcelable(ARG_COORDINATES) as Coordinates? }

    private lateinit var locationProvider: LocationProvider

    private var areAllMarkersVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationProvider = LocationProvider(requireContext())
        locationProvider.connect()
        lifecycle.addObserver(locationProvider)
    }

    override fun onDestroy() {
        lifecycle.removeObserver(locationProvider)
        super.onDestroy()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_car_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (isPermissionRequired()) {
            requestLocationPermission()
        } else {
            observeLocation()
        }
        startMap()
    }

    private fun startMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            val result = grantResults.firstOrNull { it != PackageManager.PERMISSION_GRANTED }
            if (grantResults.isNotEmpty() && result == null) {
                observeLocation()
            } else {
                Toast.makeText(requireContext(), "Permission not granted", Toast.LENGTH_SHORT).show()
            }
            startMap()
        }
    }

    private fun observeLocation() {
        // don't observe user since we came to see a specific car
        if (carCoordinates != null) {
            return
        }
        locationProvider.locationLiveData.observe(viewLifecycleOwner, Observer {
            userMarker?.let { marker ->
                marker.position = LatLng(it.latitude, it.longitude)
                if (this::map.isInitialized) {
                    jumpToLatLng(marker.position)
                }
            }
        })
        locationProvider.error.observe(viewLifecycleOwner, Observer {
            Log.e(CarMapFragment::class.java.name, it)
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun loadCarList() {
        carViewModel.getCarList()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        loadCarList()


        clusterManager = ClusterManager(requireContext(), map)
        map.setOnCameraIdleListener(clusterManager)

        map.setOnMapClickListener {
            areAllMarkersVisible = true
            setSinglePinVisibility(null, areAllMarkersVisible)
        }

        map.setOnMarkerClickListener { clickedMarker ->
            areAllMarkersVisible = areAllMarkersVisible.not()
            setSinglePinVisibility(clickedMarker, areAllMarkersVisible)
            true
        }

        if (isPermissionRequired().not()) {
            val userImage = BitmapDescriptorFactory.fromResource(R.drawable.ic_user_location)
            userMarker = clusterManager.markerCollection.addMarker(
                MarkerOptions()
                    .position(LatLng(0.0, 0.0))
                    .title(getString(R.string.your_location))
                    .icon(userImage)
            )
        }


        carViewModel.clusterCarList.observe(viewLifecycleOwner, Observer { itemList ->
            clusterManager.clearItems()
            clusterManager.addItems(itemList)
            if (carCoordinates != null) {
                jumpToCoordinates(carCoordinates)
            } else {
                // if user not granted map permission, jump the camera to the first car from the list
                if (isPermissionRequired()) {
                    jumpToCoordinates(itemList.firstOrNull()?.car?.coordinates)
                }
            }
        })
    }

    private fun jumpToLatLng(latLng: LatLng) {
        map.animateCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun jumpToCoordinates(coordinates: Coordinates?) {
        coordinates?.let { coord ->
            jumpToLatLng(
                LatLng(
                    coordinates.lat.toDouble(),
                    coordinates.lng.toDouble()
                )
            )
        }
    }

    private fun setSinglePinVisibility(currentMarker: Marker?, isVisible: Boolean) {
        clusterManager.markerCollection.markers.forEach { marker ->
            if (currentMarker?.id != marker.id) {
                marker.isVisible = isVisible
            }
        }
        clusterManager.clusterMarkerCollection.markers.forEach { marker ->
            if (currentMarker?.id != marker.id) {
                marker.isVisible = isVisible
            }
        }
        if (isVisible) {
            currentMarker?.hideInfoWindow()
        } else {
            currentMarker?.let {
                jumpToLatLng(it.position)
            }
            currentMarker?.showInfoWindow()
        }
    }

    private fun requestLocationPermission() {
        if (isPermissionRequired()) {
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        } else {
            Log.e(CarMapFragment::class.java.name, "PERMISSION GRANTED")
        }
    }

    private fun isPermissionRequired(): Boolean {
        return (ContextCompat.checkSelfPermission(
            requireContext(),
            android.Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED)
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1100
        const val ARG_COORDINATES = "arg_coordinates"
    }
}