package com.vcroitor.mtribes.demo.trasformers

interface Transformer<in T, out O> {
    fun transform(from: T): O
}