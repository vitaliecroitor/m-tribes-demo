package com.vcroitor.mtribes.demo.persistence

import androidx.room.TypeConverter
import com.vcroitor.mtribes.demo.persistence.model.QualityType

object Converters {

    @TypeConverter
    @JvmStatic
    fun qualityTypeToString(type: QualityType): String {
        return type.value
    }

    @TypeConverter
    @JvmStatic
    fun stringToQualityType(value: String): QualityType {
        return QualityType.of(value)
    }
}