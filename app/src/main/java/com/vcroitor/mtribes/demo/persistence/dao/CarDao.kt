package com.vcroitor.mtribes.demo.persistence.dao

import androidx.paging.DataSource
import androidx.room.*
import com.vcroitor.mtribes.demo.persistence.model.Car
import com.vcroitor.mtribes.demo.persistence.model.ClusterCar
import com.vcroitor.mtribes.demo.persistence.model.QualityType

@Dao
abstract class CarDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(car: Car)

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(carList: List<Car>)

    @Query("""SELECT * FROM cars ORDER BY name""")
    abstract fun getCarListDataSource(): DataSource.Factory<Int, Car>

    @Query("""SELECT * FROM cars WHERE interior=:type AND exterior=:type ORDER BY name""")
    abstract fun getCarListDataSourceByQuality(type: QualityType): DataSource.Factory<Int, Car>

    @Query("""SELECT * FROM cars ORDER BY name""")
    abstract suspend fun getCarList(): List<ClusterCar>
}