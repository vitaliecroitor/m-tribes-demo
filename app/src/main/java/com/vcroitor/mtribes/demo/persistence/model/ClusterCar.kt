package com.vcroitor.mtribes.demo.persistence.model

import androidx.room.Embedded
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ClusterCar : ClusterItem {

    @Embedded
    lateinit var car: Car

    override fun getSnippet(): String {
        return ""
    }

    override fun getTitle(): String {
        return car.name
    }

    override fun getPosition(): LatLng {
        return LatLng(car.coordinates.lat.toDouble(), car.coordinates.lng.toDouble())
    }
}