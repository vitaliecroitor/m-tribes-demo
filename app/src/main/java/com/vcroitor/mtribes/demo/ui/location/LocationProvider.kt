package com.vcroitor.mtribes.demo.ui.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Bundle
import androidx.lifecycle.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*

class LocationProvider(private val context: Context) : LifecycleObserver, GoogleApiClient.ConnectionCallbacks,
    LocationCallback(),
    GoogleApiClient.OnConnectionFailedListener {

    private val _locationLiveData = MutableLiveData<Location>()
    val locationLiveData: LiveData<Location>
        get() {
            return _locationLiveData
        }

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() {
            return _error
        }

    private val locationRequest = LocationRequest().apply {
        interval = INTERVAL
        fastestInterval = FASTEST_INTERVAL
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private val googleApiClient = GoogleApiClient.Builder(context)
        .addApi(LocationServices.API)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .build()

    fun connect(): Boolean {
        return if (isPlayServiceAvailable()) {
            googleApiClient.connect()
            true
        } else {
            false
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        stopListening()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        startListening()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        googleApiClient.disconnect()
    }

    @SuppressLint("MissingPermission")
    private fun startListening() {
        LocationServices.getFusedLocationProviderClient(context).requestLocationUpdates(locationRequest, this, null)
    }

    private fun stopListening() {
        LocationServices.getFusedLocationProviderClient(context).removeLocationUpdates(this)
    }

    override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
        locationAvailability?.let {
            if (it.isLocationAvailable.not()) {
                _error.value = "Location Not available"
            }
        }
    }

    override fun onLocationResult(location: LocationResult?) {
        location?.let {
            _locationLiveData.value = it.lastLocation
        }
    }

    private fun isPlayServiceAvailable(): Boolean {
        val status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
        return status == ConnectionResult.SUCCESS
    }

    override fun onConnected(p0: Bundle?) {
        startListening()
    }

    override fun onConnectionSuspended(p0: Int) {
        _error.value = "Connection suspended"
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        _error.value = "Connection failed"
    }

    companion object {
        const val INTERVAL = 5 * 1000L
        const val FASTEST_INTERVAL = 1 * 1000L
    }
}