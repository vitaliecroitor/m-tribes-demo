package com.vcroitor.mtribes.demo.trasformers

import com.vcroitor.mtribes.demo.api.ApiCar
import com.vcroitor.mtribes.demo.persistence.model.Car
import com.vcroitor.mtribes.demo.persistence.model.Coordinates
import com.vcroitor.mtribes.demo.persistence.model.QualityType

object CarTransformer : Transformer<ApiCar, Car> {

    override fun transform(from: ApiCar): Car {
        return Car(
            vin = from.vin,
            name = from.name,
            address = from.address,
            exterior = QualityType.of(from.exterior),
            interior = QualityType.of(from.interior),
            fuel = from.fuel,
            engineType = from.engineType,
            coordinates = Coordinates(lng = from.coordinates[0], lat = from.coordinates[1], z = from.coordinates[2])
        )
    }
}